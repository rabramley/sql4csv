import sys

class Arguments:
  """Get the options from the command line arguments"""

  def __init__(self):
    self.showHelp=False
    self.arguments = sys.argv[1:]
    self.sql=""
    self._errorMessage = ""
    self.csvFiles = {}

    for arg in self.arguments:
      if arg == "-h" or arg == "--help":
        self.showHelp = True
      elif arg[:6] == "--sql=":
        self.sql = arg[6:]
      elif arg.find('=') > -1:
        eqpos = arg.find('=')
        self.csvFiles[arg[:eqpos]] = arg[eqpos + 1:]

    if not self.sql:
      self._errorMessage += " * Mandatory parameter --sql is missing\n"

    if len(self.csvFiles) == 0:
      self._errorMessage += " * No tables and CSV files defined\n"

    if len(self._errorMessage) > 0:
      self.showHelp = True
      self._errorMessage = "Parameter errors:\n\n" + self._errorMessage

  def getHelpMessage(self):
    return self._errorMessage + """
Usage: sql4csv [OPTIONS]
    -h, --help : Display this message
    --sql='{SQL}' : Sql command
    {tablename}='{csv file path}'
      """
