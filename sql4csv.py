import sys,csv
from arguments import Arguments

args = Arguments()

if args.showHelp:
  print args.getHelpMessage()
  sys.exit(1)

print args.sql

class MyDialect(csv.Dialect):
    strict = True
    skipinitialspace = True
    quoting = csv.QUOTE_NONNUMERIC
    delimiter = ','
    quotechar = '"'
    lineterminator = '\n'

for tableName, csvFilePath in args.csvFiles.iteritems():
  with open(csvFilePath, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, MyDialect())
    for row in spamreader:
      for column in row:
        print str(column) + " is a " + type(column).__name__

sys.exit(0)

